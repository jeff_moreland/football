<?php
    if($online_result = mysqli_query($db,"SELECT * FROM users WHERE DATE_SUB(CURDATE(),INTERVAL -5 MINUTE) <= last_online")) {
         $num_users_online = mysqli_num_rows($online_result);
         while($user_online = mysqli_fetch_array($online_result)) {	
            $users_online[] = $user_online['user_name'];
         }
    }
   
?>
    </div> <!-- /container -->

   	<footer class="navbar navbar-default navbar-fixed-bottom" id="footer">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#footerbar" aria-expanded="false" aria-controls="footerbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><span id="server_status" class="glyphicon glyphicon-refresh" aria-hidden="true" style="color: green;" onclick="updateServer()"></span></a>       	
        </div>
        <div id="footerbar" class="navbar-collapse collapse">
           <ul class="nav navbar-nav">
               <li class="popover-markup">
                   <a class="triggerCredits" href="#">Server Response</a>
                   <div id="serverResponse" class="content hide">
                       <span id="txtHint"></span>
                   </div> 
               </li>
               <li>
                   <?php
                    if(isset($this_user_id)) { 
        	            if(!$my_points) { $my_points = 0; }
        	            echo '<a href="#"><span class="badge">'.$my_points.'</span> Points this week.</a>';
                    }
                   ?>
               </li>
               <li class="popover-markup">
                   <a href="#" class="trigger"><?php echo '<span class="badge">'.$num_users_online.'</span> User'.(($num_users_online > 1) ? 's' : '').' Online'; ?></a>
                   <div class="head hide">
                        Users Online
                   </div>
                   <div class="content hide">
                   <?php
                        foreach($users_online as $u) {
                            echo "$u<br>";
                        }
                   ?>     
                        
                   </div>
               </li>
               <li id="bugPopover" class="popover-markup">
                   <a href="#" class="trigger">Got <img alt="bug" src="img/glyphicons-361-bug.png" width="12px">?</a>
                   <div class="head hide">
                        Report a Bug
                   </div>
                   <div class="content hide">
                        
                        <textarea id="bug_content" name="bug_content"></textarea>
                        <input type="hidden" id="user_id" name="user_id" value="<?php echo $this_user_id; ?>">
                        <input type="hidden" id="bug_topic" name="bug_topic" value="3">
                        
                        <button onclick="submitBug()" type="button" class="btn btn-default">Report It</button> 
                   </div>
               </li>
               <li class="popover-markup">
                   <a href="#" class="trigger">Want More?</a>
                   <div class="head hide">
                        Request a Feature
                   </div>
                   <div class="content hide">
                        <textarea class="form-control" id="feature_content" name="feature_content" placeholder=""></textarea>
                        <input type="hidden" id="user_id" name="user_id" value="<?php echo $this_user_id; ?>">
                        <input type="hidden" id="feature_topic" name="feature_topic" value="4">
                        <button onclick="submitFeature()" type="button" class="btn btn-default">Request It</button>    
                   </div>
               </li>
               <li class="popover-markup">
                   <a href="#" class="triggerCredits">Credits</a>
                   <div class="head hide">
                        Features by
                   </div>
                   <div class="content hide">
                       <a href="https://github.com/BurntSushi/nfldb" target="_blank">NFLDB</a><br>
                       <a href="http://bootstrap.com" target="_blank">Bootstrap</a>
                       <a href="http://gylphicons.com" target="_blank">Gylphicons</a>
                       <a href="http://countdownjs.org" target="_blank">Countdownjs</a>
                   </div>
               </li>
           </ul> 
        </div>
      </div>
    </footer>
    <script src="js/football.js"></script>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
            var anchor = window.location.hash;
            $(anchor).collapse('toggle');
            $(anchor).parent().css('background-color', 'rgba(0, 255, 0, 0.1)');
        });
        
        $('body').on('click', function (e) {
            $('.popover-markup > .trigger').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });

        $('.popover-markup > .trigger').popover({
            html: true,
            title: function () {
                return $(this).parent().find('.head').html();
            },
            content: function () {
                return $(this).parent().find('.content').html();
            },
            placement: 'top'
        });
        $('.popover-markup > .triggerCredits').popover({
            html: true,
            title: function () {
                return $(this).parent().find('.head').html();
            },
            content: function () {
                return $(this).parent().find('.content').html();
            },
            trigger: 'focus',
            container: 'footer',
            placement: 'top'
        });


    </script>
  </body>
</html>
<?php
    mysqli_close($db);
?>