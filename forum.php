
<?php
//create_cat.php
// Load Databases and Common functions
require("mysql.php");
include 'common.php';
include("functions.php"); //Site Functions
 
//try to guess the current week, function in get_winners
guessCurrentWeek();

include 'header.php';

if(isset($this_user_name)) { 
    $_SESSION['signed_in'] = true; 
    //Update forum access date
    $result = mysqli_query($db,"UPDATE users SET last_read=NOW() WHERE user_id='$this_user_id'");
}
$con = $db;

//Forum handler functions
if(isset($_REQUEST['reply'])) {
    $sql = "INSERT INTO 
			posts(post_content,
				  post_date,
				  post_topic,
				  post_by) 
		    VALUES ('" . mysqli_real_escape_string($con, $_POST['post_content']) . "',
				NOW(),
				" . $_REQUEST['post_topic'] . ",
				" . $_REQUEST['user_id'] . ")";
						
	if($result = mysqli_query($con, $sql)) {

	   $msg = "post entered.";
	} else {
	    $msg = mysqli_error($con);
	}
    if(isset($_REQUEST['ajax'])) {
        print_r($_REQUEST);
        echo 'Content'.$_REQUEST['post_content'].' Topic ID '.$_REQUEST['post_topic'].'. '.$msg;
        exit;
    }
}
if(isset($_REQUEST['thread'])) {
    $sql = "INSERT INTO 
            topics(topic_subject,
                   topic_content,
                   topic_date,
                   topic_cat,
                   topic_by)
            VALUES('" . mysqli_real_escape_string($con, $_POST['topic_subject']) . "',
                   '" . mysqli_real_escape_string($con, $_POST['topic_content']) . "',
                   NOW(),
                   '" . mysqli_real_escape_string($con, $_POST['topic_cat']) . "',
                   '" . $_POST['user_id'] . "'
                   )";
						
	if($result = mysqli_query($con, $sql)) {
	    //echo "post entered.";
	} else {
	    //echo mysqli_error($con);
	}
}
$sql = "SELECT
			categories.cat_id,
			categories.cat_name,
			categories.cat_description,
			COUNT(topics.topic_id) AS topics
		FROM
			categories
		LEFT JOIN
			topics
		ON
			topics.topic_id = categories.cat_id
		GROUP BY
			categories.cat_name, categories.cat_description, categories.cat_id";        


$result = mysqli_query($con, $sql);

if(!$result) {
	echo 'The categories could not be displayed, please try again later.';
} else {
	if(mysqli_num_rows($result) == 0) {
		echo 'No categories defined yet.';
	} else {
		//prepare the table
        echo '
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"><!-- Accordion -->';
		while($row = mysqli_fetch_array($result)) {		
            echo '
    <div class="panel panel-default"><!-- Category Panel -->
        <div class="panel-heading" role="tab" id="headingCat'.$row["cat_id"].'" style="background-color: rgba(108, 126, 255, 0.5);"><!-- Category Head -->
            <h3 class="panel-title categoryTitle">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCat'.$row["cat_id"].'" aria-expanded="true" aria-controls="collapseCat'.$row["cat_id"].'">'
                    .$row["cat_name"].
                '</a>
            </h3>
        </div><!-- End Category Head -->
        <div id="collapseCat'.$row["cat_id"].'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingCat'.$row["cat_id"].'"><!-- Category Collapse -->
            <div class="panel-body categoryContent">
                '.$row["cat_description"].'
            </div>
            <ul class="list-group"><!-- Topic List -->';
                  							
			            $topicsql = "SELECT
							            topic_id,
							            topic_subject,
                                        topic_content,
							            topic_date,
							            topic_cat,
                                        users.user_id,
						                users.user_name
						            FROM
							            topics
                                    LEFT JOIN
						                users
					                ON
						                topics.topic_by = users.user_id
						            WHERE
							            topic_cat = '" . $row['cat_id'] . "'
						            ORDER BY
							            topic_date
						            DESC";
								
			            $topicsresult = mysqli_query($con, $topicsql);
				
			            if(!$topicsresult) {
				            echo '
                <li class="list-group-item">Last topic could not be displayed.</li>';
			            }
			            else {
				            if(mysqli_num_rows($topicsresult) == 0) {
					            echo '
                <li class="list-group-item">No threads posted yet. Start one now.</li>';
				            } else {
					            while($topicrow = mysqli_fetch_array($topicsresult)) {
					                echo '
                <li class="list-group-item" style="background-color: rgba(240, 240, 240, '.(($topicrow['topic_id']%2==0) ? '0.45':'0.65').')"><a data-toggle="collapse" href="#collapseTopic'.$topicrow['topic_id'].'"><h4 class="topicTitle">'.$topicrow['topic_subject'].'</h4></a>';
                //$sql = 
                $date_result = mysqli_query($con, "SELECT MAX(post_date) as post_date FROM posts WHERE post_topic=".$topicrow['topic_id']);
                if($date_result) {
                    $last_post_date = mysqli_fetch_array($date_result)['post_date'];
                } else {
                    $last_post_date = $topicrow['topic_date'];
                }
                                    echo '
                <div class="collapse'.((strtotime($this_last_read)<strtotime($last_post_date)) ? ' in' : '').'" id="collapseTopic'.$topicrow["topic_id"].'"><blockquote class="topicContent">'.$topicrow['topic_content'].'<br><small>'.$topicrow['user_name'].' (' . date('D, M j Y H:i:s T', strtotime($topicrow['topic_date'])) . ')</small></blockquote>';

                                    //fetch the posts from the database
			                        $posts_sql = "SELECT
						                            posts.post_topic,
						                            posts.post_content,
						                            posts.post_date,
						                            posts.post_by,
                                                    users.user_id,
						                            users.user_name
					                            FROM
						                            posts
					                            LEFT JOIN
						                            users
					                            ON
						                            posts.post_by = users.user_id
					                            WHERE
						                            posts.post_topic = ".$topicrow['topic_id'];
						            //echo "$posts_sql";
			                        $posts_result = mysqli_query($con, $posts_sql); 

                                    if($posts_result) {
                                        echo '
                    <ul class="list-group posts" style="text-indent: 4px;"><!-- Post list -->';
                                        while($posts_row = mysqli_fetch_array($posts_result)) {
                                            echo '
                        <li class="list-group-item" style="background-color: inherit;"><blockquote class="postContent" style="background-color: '.((strtotime($this_last_read)<strtotime($posts_row['post_date'])) ? 'rgba(0, 0, 255, 0.1)' : 'inherit').';">'.htmlentities(stripslashes($posts_row['post_content'])).'<br><small>'.$posts_row['user_name'].' ('.date('D, M j Y H:i:s T', strtotime($posts_row['post_date'])).')</small></blockquote></li>';
				                        }
                                    
                                    }
                                    echo '
                    </ul></div><!-- End Post List -->
                    &#8627; <a href="#topicReply'.$topicrow['topic_id'].'" data-toggle="collapse" aria-expanded="false"><small>Post a reply</small></a>
                            <form id="topicReply'.$topicrow['topic_id'].'" method="post" action="forum.php" class="collapse">
                            <textarea class="form-control" name="post_content" placeholder="Enter a reply"></textarea>
                            <input type="hidden" name="user_id" value="'.$this_user_id.'">
                            <input type="hidden" name="post_topic" value="'.$topicrow['topic_id'].'">
                            <button type="submit" class="btn btn-default" name="reply">Reply</button>
                            </form>
                </li>';

		                        }
	                        }
                        }
                        echo '
                <li class="list-group-item">
                    <a href="#topicThread'.$row['cat_id'].'" data-toggle="collapse" aria-expanded="false">Start a new thread</a>
                    <form id="topicThread'.$row['cat_id'].'" method="post" action="forum.php" class="collapse">
                    <input type="text" class="form-control" name="topic_subject" placeholder="Subject">
                    <textarea class="form-control" name="topic_content" placeholder="Thread content"></textarea>
                    <input type="hidden" name="user_id" value="'.$this_user_id.'">
                    <input type="hidden" name="topic_cat" value="'.$row['cat_id'].'">
                    <button type="submit" class="btn btn-default" name="thread">Post</button>
                    </form>
                </li>
            </ul><!-- End Topic List -->
        </div><!-- End Category Collapse -->
    </div><!-- End Panel -->';
        }
        echo '
</div><!-- Accordian -->';  
    }     
}
?>

<?php
include 'footer.php';

?>