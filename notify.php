<?php //Email notification script

 //Search the users in the db for anyone with at leat one game that is not picked
 //Count games in week
 //Count games user has picked
 //compare
 //If picked < games || score IS NULL) = send email
 // Load Databases and Common functions
require("mysql.php");
include('common.php');
include("functions.php"); //Site Functions
//include('include/test_include.php');
//try to guess the current week, function in get_winners
guessCurrentWeek();
/*
SELECT  user_id, group_id
FROM    g_members
WHERE   NOT EXISTS     
(     
       SELECT picks.user_id   
       FROM    picks       
       WHERE   picks.user_id = g_members.user_id  AND picks.season_year=2015 AND picks.season_type='Regular' AND picks.week=1
)
*/
//Notify all users that have made no picks
$sql = "SELECT user_id, group_id\n"
    . "FROM g_members\n"
    . "WHERE group_id=6 AND NOT EXISTS \n"
    . "( \n"
    . " SELECT picks.user_id \n"
    . " FROM picks \n"
    . " WHERE picks.user_id = g_members.user_id AND picks.season_year='$this_season_year' AND picks.season_type='$this_season_type' AND picks.week='$this_week'\n"
    . ")";
$result = mysqli_query($db, $sql) or die(mysqli_error($db));
while($user = mysqli_fetch_array($result)) {
    echo '<p>User '.getUserNameFromId($db,$user['user_id']).' ('.$user['user_id'].') has made no picks this week for Group '.$user['group_id'].'. Send an email to '.getUserEmailFromId($db,$user['user_id']).'</p>';
    if(isset($_REQUEST['type']) && $_REQUEST['type']=='email') {
        notifyUser(getUserEmailFromId($db,$user['user_id']),getUserNameFromId($db,$user['user_id']),$num_games,$user['num_picks']);
    }
}
//Notify users that have made some but not all picks yets
$num_games = getNumberOfGames($this_season_year,$this_season_type,$this_week);
//$sql = "SELECT COUNT(pick_id) as num_picks, user_id FROM picks WHERE season_year='$this_season_year' AND season_type='$this_season_type' AND week='$this_week' GROUP BY user_id";
$sql = "SELECT COUNT(picks.pick_id) as num_picks, picks.user_id, g_members.group_id \n"
    . "FROM picks \n"
    . "JOIN g_members\n"
    . "ON picks.user_id = g_members.user_id\n"
    . "WHERE season_year='$this_season_year' AND season_type='$this_season_type' AND week='$this_week'\n"
    . "GROUP BY picks.user_id";
$result = mysqli_query($db, $sql) or die(mysqli_error($db));
while($user = mysqli_fetch_array($result)) {
    if($user['num_picks'] < $num_games) {
        echo '<p>User '.getUserNameFromId($db,$user['user_id']).' ('.$user['user_id'].') has only picked '.$user['num_picks'].' of '.$num_games.' for Group '.$user['group_id'].'. Send an email to '.getUserEmailFromId($db,$user['user_id']).'</p>';
        if(isset($_REQUEST['type']) && $_REQUEST['type']=='email') {
            notifyUser(getUserEmailFromId($db,$user['user_id']),getUserNameFromId($db,$user['user_id']),$num_games,$user['num_picks']);
        }
    }
}

?>
