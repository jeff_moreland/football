
<?php
//create_cat.php
// Load Databases and Common functions
require("mysql.php");
include 'common.php';
include("functions.php"); //Site Functions
 
//try to guess the current week, function in get_winners
guessCurrentWeek();

include 'header.php';

if(isset($this_user_id)) {
  echo '<form class="form-signin" action="login.php" method="post">
            <h2>Update Profile</h2> 
            <label for="inputEmail">Email address</label>
            <input type="email" id="inputEmail" name="user_email" class="form-control" placeholder="'.$this_user_email.'" disabled>
            <label for="inputPassword">Change Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Enter New Password">
            <input type="password" id="inputPassword2" name="password2" class="form-control" placeholder="Re-Enter New Password">
            <label for="inputUserName">Change User Name</label>
            <input type="text" id="inputUserName" name="user_name" class="form-control" value="'.$this_user_name.'">
            <button class="btn btn-lg btn-primary btn-block" type="submit" id="registerButton" name="update" value="user">Update Profile</button>
    </form>';
}

include 'footer.php';
?>