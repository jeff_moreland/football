<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="Jeff Moreland <jeff@evose.com>">
    <link rel="icon" href="football.ico">

    <title>Football Selection Site</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="css/navbar-fixed-top.css" rel="stylesheet"> -->
    <!-- <link href="css/signin.css" rel="stylesheet"> -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
    <link href="css/football.css" rel="stylesheet">

    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- <script src="https://smalldo.gs/js/countdown.js"></script> -->
    <script src="js/countdown.min.js"></script>
    
    <script src="js/holder.js"></script>
    <?php if($SITE_PAGE=='picks' && ((date("N")==7 && date("G")>=13) || (date("N")==1) || (date("N")==4 && date("G")>18))) { echo '<script type="text/javascript">window.setTimeout(function(){ document.location.reload(true); }, 60000);</script>'; } ?>

  </head>

  <body onload="updateServer()">

<?php 

    include("navigator.php");
    if(isset($this_user_name)) { include("selector.php"); }

    echo "<div class=\"container\">\n";

    if(time()<strtotime("12:00am September 20 2015")) {
        $this_message = 'New features have been added to the site. Check out the <a href="forum.php#collapseTopic5">Forum</a> to see what is new.';
    }
    if(time()<strtotime("01:00PM October 11 2015")) {
        $this_message = 'RULE CHANGE: Effective this week, all games will lock at 1pm EDT on Sunday.';
    }
    if(isset($this_message)) {
        //display message alert
        echo '<div class="alert alert-warning alert-dismissible" role="alert">
  	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.
  	    $this_message.'</div>';
    }
 ?>
