<?php //Site Help Carousel
    
//--LOAD SITE HEADER
// Load Databases and Common functions
require("mysql.php");
include 'common.php';
include("functions.php"); //Site Functions
 
//try to guess the current week, function in get_winners
guessCurrentWeek();

if(date("N") >= 4 || date("N") == 1) {
    $show_week = $this_week;
} else {
    $show_week = $this_week - 1;
}

$this_college = 'South Carolina';
include("header.php");
//--END SITE HEADER
?>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="12000">
  <!-- Indicators -->
  <ol class="carousel-indicators" style="margin-bottom: 0px;">
    <li data-target="#carousel-example-generic" data-slide-to="0"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
      <div class="item" style="background-image: url('img/C.Shaw.jpg')">
          <!--<img src="img/C.Shaw.jpg" alt="Passing">-->
          <h3 class="statsTitle">Week <?php echo $show_week; ?> Passing Leaders</h3>  
          <div class="statsContainer">
            <table class="statsTable">
                <tr><th>Name</th><th>Team</th><th>CMP</th><th>ATT</th><th>YDS</th><th>TD</th><th>INT</th></tr>
            <?php

                $passing_stats = getPassingStats($this_college,$this_season_year,$this_season_type,$show_week); 

                if($passing_stats) {
                    foreach($passing_stats as $stat) {
                        echo '<tr><td>'.$stat['full_name'].'</td><td>'.$stat['team'].'</td><td>'.$stat['passing_cmp'].'</td><td>'.$stat['passing_att'].'</td><td>'.$stat['passing_yds'].'</td><td>'.$stat['passing_tds'].'</td><td>'.$stat['passing_int'].'</td></tr>';    
                    }   
                } else {
                    echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>\n";
                }
            ?>
                <!--<tr><td>D.Watson</td><td>QB</td><td colspan="6">Coming in 2017</td></tr>-->
            </table> 
        </div>
      </div>
 
      <div class="item active" style="background-image: url('img/A.Jeffery.jpg');">
        <!--<img src="img/A.Jeffery.jpg" alt="Receiving">-->
        <h3 class="statsTitle">Week <?php echo $show_week; ?> Receiving Leaders</h3>    
        <div class="statsContainer">        
            <table class="statsTable">
                <tr><th>Name</th><th>Pos</th><th>Team</th><th>REC</th><th>YDS</th><th>AVG</th><th>TD</th></tr>
            <?php

                $receiving_stats = getReceivingStats($this_college,$this_season_year,$this_season_type,$show_week); 

                if($receiving_stats) {
                    foreach($receiving_stats as $stat) {
                        echo '<tr><td>'.$stat['full_name'].'</td><td>'.$stat['position'].'</td><td>'.$stat['team'].'</td><td>'.$stat['receiving_rec'].'</td><td>'.$stat['receiving_yds'].'</td><td>'.round($stat['receiving_yds']/$stat['receiving_rec'],1).'</td><td>'.$stat['receiving_tds'].'</td></tr>';    
                    }  
                }
            ?>
            </table>
        </div>
      </div>

      <div class="item" style="background-image: url('img/M.Davis.jpg');">
          <h3 class="statsTitle">Week <?php echo $show_week; ?> Rushing Leaders</h3>
          <div class="statsContainer">
            <table class="statsTable">
                <tr><th>Name</th><th>Pos</th><th>Team</th><th>ATT</th><th>YDS</th><th>AVG</th><th>TD</th></tr>
            <?php

                $rushing_stats = getRushingStats($this_college,$this_season_year,$this_season_type,$show_week); 

                if($rushing_stats) {
                    foreach($rushing_stats as $stat) {
                        echo '<tr><td>'.$stat['full_name'].'</td><td>'.$stat['position'].'</td><td>'.$stat['team'].'</td><td>'.$stat['rushing_att'].'</td><td>'.$stat['rushing_yds'].'</td><td>'.round($stat['rushing_yds']/$stat['rushing_att'],1).'</td><td>'.$stat['rushing_tds'].'</td></tr>';    
                    }  
                }
            ?>
            </table>
          </div>
      </div>

      <div class="item" style="background-image: url('img/J.Clowney.jpg');">
        <h3 class="statsTitle">Week <?php echo $show_week; ?> Defensive Leaders</h3> 
        <div class="statsContainer">
            <table class="statsTable">
                <tr><th>Name</th><th>Pos</th><th>Team</th><th>TKL</th><th>SAC</th><th>TFL</th><th>INT</th></tr>
            <?php

                $defense_stats = getDefenseStats($this_college,$this_season_year,$this_season_type,$show_week); 
                  
                if($defense_stats) {
                    $i=0;
                    foreach($defense_stats as $stat) {
                        if($i<6) {
                            echo '<tr><td>'.$stat['full_name'].'</td><td>'.$stat['position'].'</td><td>'.$stat['team'].'</td><td>'.$stat['defense_tkl'].'</td><td>'.$stat['defense_sk'].'</td><td>'.$stat['defense_tkl_loss'].'</td><td>'.$stat['defense_int'].'</td></tr>';    
                        }
                        $i++;
                    }  
                }
            ?>
            </table>
        </div>
      </div>
  </div>
  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

</div>

<h3>Season Stats</h3>

<div class="panel panel-default stats">
  <!-- Default panel contents -->
  <div class="panel-heading"><a data-toggle="collapse" href="#collapse1">Active Roster
  <span class="badge">
  <?php
        //$active_roster = getClemsonActivePlayers(); 
        $active_roster = getActivePlayers($this_college);
        if($active_roster) {
            echo count($active_roster);  
        } else {
            echo "0";
        }
    ?>
  </span></a></div>
  <div id="collapse1" class="panel-collapse collapse">
  <!-- Table -->
  <table class="table">
    <tr><th>Name</th><th>Pos</th><th>Team</th></tr>
    <?php
        //$active_roster = getClemsonActivePlayers(); 
        $active_roster = getActivePlayers($this_college);
        if($active_roster) {
            foreach($active_roster as $active) {
                echo '<tr><td>'.$active['full_name'].'</td><td>'.$active['position'].'</td><td>'.$active['team'].'</td></tr>';    
            }   
        } else {
            echo "No Active Clemson Players.\n";
        }
    ?>
  </table>
  </div>
</div>

<div class="panel panel-default stats">
  <!-- Default panel contents -->
  <div class="panel-heading">Passing</div>
  <!-- Table -->
  <table class="table">
    <tr><th>Name</th><th>Pos</th><th>Team</th><th>CMP</th><th>ATT</th><th>YDS</th><th>TD</th><th>INT</th></tr>
    <?php
        $passing_stats = getPassingStats($this_college,$this_season_year,$this_season_type); 

        if($passing_stats) {
            foreach($passing_stats as $stat) {
                echo '<tr><td>'.$stat['full_name'].'</td><td>'.$stat['position'].'</td><td>'.$stat['team'].'</td><td>'.$stat['passing_cmp'].'</td><td>'.$stat['passing_att'].'</td><td>'.$stat['passing_yds'].'</td><td>'.$stat['passing_tds'].'</td><td>'.$stat['passing_int'].'</td></tr>';    
            }   
        } else {
            echo "";
        }
    ?>
  </table>
</div>
<div class="panel panel-default stats">
  <!-- Default panel contents -->
  <div class="panel-heading">Receiving</div>
  <!-- Table -->
  <table class="table">
    <tr><th>Name</th><th>Pos</th><th>Team</th><th>REC</th><th>YDS</th><th>AVG</th><th>TD</th></tr>
    <?php
        $receiving_stats = getReceivingStats($this_college,$this_season_year,$this_season_type); 

        if($receiving_stats) {
            foreach($receiving_stats as $stat) {
                echo '<tr><td>'.$stat['full_name'].'</td><td>'.$stat['position'].'</td><td>'.$stat['team'].'</td><td>'.$stat['receiving_rec'].'</td><td>'.$stat['receiving_yds'].'</td><td>'.round($stat['receiving_yds']/$stat['receiving_rec'],1).'</td><td>'.$stat['receiving_tds'].'</td></tr>';    
            }  
        }
    ?>
  </table>
</div>
<div class="panel panel-default stats">
  <!-- Default panel contents -->
  <div class="panel-heading">Rushing</div>
  <!-- Table -->
  <table class="table">
    <tr><th>Name</th><th>Pos</th><th>Team</th><th>ATT</th><th>YDS</th><th>AVG</th><th>TD</th></tr>
    <?php
        $rushing_stats = getRushingStats($this_college,$this_season_year,$this_season_type); 

        if($rushing_stats) {
            foreach($rushing_stats as $stat) {
                echo '<tr><td>'.$stat['full_name'].'</td><td>'.$stat['position'].'</td><td>'.$stat['team'].'</td><td>'.$stat['rushing_att'].'</td><td>'.$stat['rushing_yds'].'</td><td>'.round($stat['rushing_yds']/$stat['rushing_att'],1).'</td><td>'.$stat['rushing_tds'].'</td></tr>';    
            }  
        }
    ?>
  </table>
</div>
<div class="panel panel-default stats">
  <!-- Default panel contents -->
  <div class="panel-heading">Defense</div>
  <!-- Table -->
  <table class="table">
    <tr><th>Name</th><th>Pos</th><th>Team</th><th>TKL</th><th>SAC</th><th>TFL</th><th>INT</th></tr>
    <?php
        $defense_stats = getDefenseStats($this_college,$this_season_year,$this_season_type); 
                  
        if($defense_stats) {
            foreach($defense_stats as $stat) {
                echo '<tr><td>'.$stat['full_name'].'</td><td>'.$stat['position'].'</td><td>'.$stat['team'].'</td><td>'.$stat['defense_tkl'].'</td><td>'.$stat['defense_sk'].'</td><td>'.$stat['defense_tkl_loss'].'</td><td>'.$stat['defense_int'].'</td></tr>';    
            }  
        }
    ?>
  </table>
</div>
<div class="panel panel-default stats">
  <!-- Default panel contents -->
  <div class="panel-heading">Kicking</div>
  <!-- Table -->
  <table class="table">
    <tr><th>Name</th><th>Pos</th><th>Team</th><th>FGM/A</th><th>AVG</th><th>LNG</th><th>XPM/A</th></tr>
    <?php
        $kicking_stats = getKickingStats($this_college,$this_season_year,$this_season_type); 
                  
        if($kicking_stats) {
            foreach($kicking_stats as $stat) {
                echo '<tr><td>'.$stat['full_name'].'</td><td>'.$stat['position'].'</td><td>'.$stat['team'].'</td><td>'.$stat['kicking_fgm'].'/'.$stat['kicking_fga'].'</td><td>'.round($stat['kicking_fgm_yds']/$stat['kicking_fgm'],2).'</td><td>'.$stat['kicking_fgm_long'].'</td><td>'.$stat['kicking_xpmade'].'/'.$stat['kicking_xpa'].'</td></tr>';    
            }  
        }
    ?>
  </table>
</div>
<div class="panel panel-default stats">
  <!-- Default panel contents -->
  <div class="panel-heading">Punting</div>
  <!-- Table -->
  <table class="table">
    <tr><th>Name</th><th>Pos</th><th>Team</th><th>AVG</th><th>LNG</th><th>IN20</th></tr>
    <?php
        $punting_stats = getPuntingStats($this_college,$this_season_year,$this_season_type); 
                  
        if($punting_stats) {
            foreach($punting_stats as $stat) {
                echo '<tr><td>'.$stat['full_name'].'</td><td>'.$stat['position'].'</td><td>'.$stat['team'].'</td><td>'.round($stat['punting_yds']/$stat['punting_tot'],2).'</td><td>'.$stat['punting_yds_long'].'</td><td>'.$stat['punting_i20'].'</td></tr>';    
            }  
        }
    ?>
  </table>
</div>
<?php
//--Main Footer
include 'footer.php';

?>